let respuestasCorrectas = 0;

const contenedor = document.getElementById("test");
const botonRes = document.getElementById("boton");
const resultadoTest = document.getElementById("resultado")

const preguntas = [
    {
      pregunta: "1. ¿Que fue el Bismarck?",
      respuestas: {
        a: "Fue un acorazados buque Aleman.",
        b: "Fue un navio del batallom Alestorm.",
        c: "Es una maniobra que emplearon los Alemanes."
      },
      respuestaCorrecta: 'a'
    },
    {
      pregunta: "2. ¿Que vandos formaban la guerra?",
      respuestas: {
        a: "Las potencias del Imperio y Los países Aliados.",
        b: "Los paises de Gondor y Las potencias de Rivendel.",
        c: "Las potencias del Eje y Los países Aliados."
      },
      respuestaCorrecta: "c"
    },
    {
      pregunta: "3. ¿Como se conocia al Regimiento Ruso de Bombardeo Nocturno 588.º.?",
      respuestas: {
        a: "Batallon Aereo 588.",
        b: "Fuerzas Aereas 588.",
        c: "Brujas nocturnas.",
        d: "Eparacooooo."
      },
      respuestaCorrecta: "c"
    },
    {
      pregunta: "4. ¿Que gran accion realizo el batallon Belga Cazadores de las Ardenas (Chasseurs Ardennais)?",
      respuestas: {
        a: "Defender la frontera Belga con una ferocidad que asusto a los enemigos.",
        b: "Usar maniobras aereas imposibles.",
        c: "Tener uno de los mejores pilotos de aviones Rote Kampfflieger."
      },
      respuestaCorrecta: "a"
    },
    {
        pregunta: "5. ¿Que día dio comiezo el día D?",
        respuestas: {
            a: "9 de octubre de 1989.",
            b: "Nunca, eso no existe, solo existe el día V.",
            c: "6 de junio de 1944."
      },
      respuestaCorrecta: "c"
    },
    {
      pregunta: "6. Quien dijo: Os dieron a elegir entre el deshonor o la guerra, elegisteis el deshonor y tendreis la guerra.",
      respuestas: {
        a: "William Wallace.",
        b: "El emperador Palpatine.",
        c: "Winston Churchill."
      },
      respuestaCorrecta: "c"
    }
];

function mostrarTest(){
  const preguntasYrespuestas = [];

  preguntas.forEach((preguntaActual, numeroDePregunta) => {
    const respuestas = [];
    for(letraRespuesta in preguntaActual.respuestas){
      respuestas.push(
        `<label>
              <input type="radio" name"${numeroDePregunta}" value="${letraRespuesta}">
              ${letraRespuesta} : ${preguntaActual.respuestas[letraRespuesta]}
        </lebel>`
      );
    }
      preguntasYrespuestas.push(
        `<div class="cuestion"> ${preguntaActual.pregunta} </div>
         <div class="respuestas"> ${respuestas.join('')} </div>
        `
   );
 });

contenedor.innerHTML = preguntasYrespuestas.join('');
}
mostrarTest();

function mostrarResultado(){
  const respuestas = contenedor.querySelectorAll(".respuestas");
  let respuestaCorrecta = 0;

  preguntas.forEach((preguntaActual, numeroDePregunta) => {
    const todasLasRespuestas = respuestas[numeroDePregunta];
    const checkboxRespuestas = `imput[name='${numeroDePregunta}']:checked`;
    const respuestaElegida = (todasLasRespuestas.querySelector(checkboxRespuestas) || {}).value;
    document.getElementById("a").innerHTML = preguntaActual.respuestaCorrecta;


    if(respuestaElegida == preguntaActual.respuestaCorrecta){
      respuestasCorrectas++;
    }
  });

//resultadoTest.innerHTML = 'Toma ' + respuestasCorrectas + ' de ' + preguntas.length;
alert('Toma ' + respuestasCorrectas + ' de ' + preguntas.length);
	//alert(indicie_respuesta_correcta);
}
botonRes.addEventListener('click', mostrarResultado);
